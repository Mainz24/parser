package org.example;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

public class CSVParser {
    public static void main(String[] args) {
        String csvFilePath = "logs_nginx.csv";
        String outputFilePath = "parser.csv";

        try (CSVReader reader = new CSVReader(new FileReader(csvFilePath));
             FileWriter writer = new FileWriter(outputFilePath);
             CSVWriter csvWriter = new CSVWriter(writer)) {

            String[] nextLine;
            while ((nextLine = reader.readNext()) != null) {
                String logEntry = nextLine[0];

                Pattern pattern = Pattern.compile("\\[(.*?)\\] \"(.*?) (.*?)\"");
                Matcher matcher = pattern.matcher(logEntry);
                if (matcher.find()) {
                    String date = matcher.group(1);
                    String method = matcher.group(2);
                    String path = matcher.group(3);

                    String[] requestPath = new String[]{path};
                    String[] requestDate = new String[]{date};
                    String[] request1 = requestPath[0].split(" ");
                    String methodDate = requestDate[0];
                    String newDate = methodDate.substring(0, methodDate.length() - 6);
                    String path1 = request1[0].replaceAll("\\d+", "");
                    String newPath = path1.substring(0, path1.lastIndexOf('/')) + "/";

                    csvWriter.writeNext(new String[]{newDate, method, newPath});
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
